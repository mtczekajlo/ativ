# ASCII Terminal Image Viewer

ATIV (ASCII Terminal Image Viewer) is a naive tool to be used from within your terminal to print out images as ASCII characters. It scales to your terminal size - the smaller the font, the nicer ASCII art you get.
