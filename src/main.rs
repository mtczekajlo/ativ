use clap::{arg, command};
use image::io::Reader as ImageReader;
use std::error::Error;

const GREY_SCALE: &[u8] =
    b" .'`^\",:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$";

fn main() -> Result<(), Box<dyn Error>> {
    let matches = command!()
        .about("ASCII Terminal Image Viewer")
        .arg(arg!([IMAGE]).required(true))
        .get_matches();
    let (w, h) = term_size::dimensions().expect("Unable to get term size!");
    let img = ImageReader::open(matches.get_one::<String>("IMAGE").unwrap())?.decode()?;
    let img = img.resize(w as u32, h as u32, image::imageops::FilterType::Nearest);
    let mut img = img.grayscale().into_luma8();
    for (_, _, p) in img.enumerate_pixels_mut() {
        *p = image::Luma([(p.0[0] as f32 / 255f32 * 69f32) as u8]);
    }
    for row in img.enumerate_rows() {
        for col in row.1 {
            print!("{}", GREY_SCALE[col.2[0] as usize] as char);
        }
        println!();
    }
    println!();
    Ok(())
}
